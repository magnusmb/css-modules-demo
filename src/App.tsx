import React, { useEffect, useState } from "react";
import "./App.css";
import style from "./App.module.css";
import { NewTodoForm } from "./components/NewTodoForm";
import { Todo, TodosList } from "./components/TodosList";

let todoCount = 0;

function App() {
  const [todos, setTodos] = useState<Todo[]>([]);

  function onAddTodo(text: string) {
    const newTodo = {
      id: todoCount++,
      text,
      done: false,
    };
    setTodos([...todos, newTodo]);
  }

  function onUpdateTodo(thisTodo: Todo) {
    setTodos(
      todos.map((aTodo) => (aTodo.id === thisTodo.id ? thisTodo : aTodo))
    );
  }

  return (
    <div className={style.main}>
      <NewTodoForm onAddTodo={onAddTodo} />
      <TodosList todos={todos} onUpdateTodo={onUpdateTodo} />
    </div>
  );
}

export default App;

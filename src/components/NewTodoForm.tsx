import React, { useState } from "react";

import style from "./NewTodoForm.module.css";

interface NewTodoFormProps {
  onAddTodo(text: string): void;
}

export function NewTodoForm(props: NewTodoFormProps) {
  const [todoText, setTodoText] = useState<string>("");

  function addTodo() {
    if (todoText) {
      props.onAddTodo(todoText);
      setTodoText("");
    }
  }

  return (
    <input
      className={style.text}
      type="text"
      value={todoText}
      placeholder="Write your todo here"
      onChange={(e) => setTodoText(e.target.value)}
      onKeyDown={(e) => e.key === "Enter" && addTodo()}
    />
  );
}

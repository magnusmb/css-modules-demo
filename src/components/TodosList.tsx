import React from "react";

import style from "./TodosList.module.css";

export interface Todo {
  id: number;
  text: string;
  done: boolean;
}

interface TodosListProps {
  todos: Todo[];
  onUpdateTodo(todo: Todo): void;
}

const catenate = (classes: Array<string | boolean>) =>
  classes.filter((name) => !!name).join(" ");

export function TodosList(props: TodosListProps) {
  function toggleTodo(todo: Todo) {
    props.onUpdateTodo({ ...todo, done: !todo.done });
  }

  function changeTodo(todo: Todo, newText: string) {
    props.onUpdateTodo({ ...todo, text: newText });
  }

  return (
    <ul className={style.list}>
      {props.todos.map((todo, index) => (
        <li key={index} className={todo.done ? style.itemChecked : style.item}>
          <input
            className={style.checkbox}
            type="checkbox"
            checked={todo.done}
            onChange={() => toggleTodo(todo)}
          />
          <input
            className={style.input}
            type="text"
            value={todo.text}
            onChange={(e) => changeTodo(todo, e.target.value)}
          />
        </li>
      ))}
    </ul>
  );
}
